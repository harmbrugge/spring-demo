<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%--
  Created by IntelliJ IDEA.
  User: hbrugge
  Date: 12-1-16
  Time: 10:44
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>

<form:form action="/user" method="post" enctype="multipart/form-data" commandName="user">

    username: <form:input type="text" path="username"/><form:errors path="username"/><br/>
    name: <form:input type="text" path="name"/><br/>
    mail: <form:input type="text" path="mail"/><br/>
    password <form:input type="password" path="password"/><form:errors path="password"/> <br/>
    file: <form:input type="file" path="file"/><br/>

    <button type="submit">
        Submit
    </button>

</form:form>

</body>
</html>
